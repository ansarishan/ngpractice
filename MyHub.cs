﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Timers;
using Microsoft.AspNet.SignalR;
using System.Messaging;

namespace ngPractice
{
    public class MyHub : Hub
    {
        private static Timer aTimer;

        public void FirstHello()
        {
            Clients.All.FirstMsg("Hello first signalR message!");

            aTimer = new Timer(5);
            aTimer.Enabled = true;
            aTimer.Elapsed += OnTimedEvent;

        }

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            Clients.All.firstPoolingServ("Hello server pooling " + DateTime.Now.ToString("mm:ss") + "!");
        }

        public void HelloPooling()
        {
            Clients.All.FirstPooling("Hello client pooling "+DateTime.Now.ToString("mm:sss")+"!");
        }

        public void MyQueue()
        {


            string MessageQueuePath = ".\\Private$\\MyQueue";
            // it is important to set this to false, otherwise the message receiver event
            // handler keeps still active even after closing the message queue object
            MessageQueue.EnableConnectionCache = false;
            // open the selected message queue
            MessageQueue MQueue = new MessageQueue(MessageQueuePath);
            MQueue.MessageReadPropertyFilter.SetAll();
            // the target types we have stored in the message body
            MQueue.Formatter = new XmlMessageFormatter(new String[] { "System.String,mscorlib" });
            // set the event handler to be called when the message has been received
            MQueue.ReceiveCompleted += new ReceiveCompletedEventHandler(MessageEventHandler);
            // start the receive message process; this call returns immediately
            IAsyncResult MQResult = MQueue.BeginReceive(new TimeSpan(1, 0, 0), MQueue);



        }
        /// <summary>
        /// event handler called when the message is ready to be read
        /// </summary>
        private void MessageEventHandler(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
                // get the message from the queue
                System.Messaging.Message Msg = ((MessageQueue)e.AsyncResult.AsyncState).EndReceive(e.AsyncResult);
            
                // broadcast the message
                Clients.All.MsgMyQueue(Msg.Body.ToString());//Msg.Body.ToString());
                // start looking for the next message
                IAsyncResult AsyncResult = ((MessageQueue)e.AsyncResult.AsyncState).BeginReceive(new TimeSpan(1, 0, 0), ((MessageQueue)e.AsyncResult.AsyncState));
            }
            catch (Exception ex)
            {

            }


        }

    }
}