﻿(function () {
    'use strict';

    var app = angular.module('myApp', [
         ]);

    angular.module('myApp').controller('myController', ['$scope', '$interval', FirstController]);



    angular.module('myApp').directive('myGraph', function () {
        
        return {
            restrict: 'E',
            replace: true,
            template: '<div class="panel panel-primary" style="width:{{graphWidth}}px;height:{{graphHeight}}px" >' +
                      '<div class="panel-heading">{{graphTitle}}</div>' +
                      '<div class="panel-body"  >' +
                      '<canvas id="myCanvas"></canvas>' +
                      '</div>' +
                      '</div>',
            scope: {
                graphWidth: '@',
                graphHeight: '@',
                graphTitle: "@",
                graphData: '=graphData'
            },
            link: function (scope, elem, attr) {
                scope.graphWidth = 300;
                scope.graphHeight = 300;

                scope.graphTitle = "";
               // scope.graphData =[{ 'name': 'Bar A', 'y_axis': '50' }, { 'name': 'Bar B', 'y_axis': '10' } ];

                var c = elem.find('canvas')[0];
                c.width = scope.graphWidth-30;
                c.height = scope.graphHeight-80;
                var ctx = c.getContext("2d");
                var x_space = 20;


            
                ctx.strokeStyle = "blue";
                ctx.moveTo(0, scope.graphHeight-100);
                ctx.lineTo(scope.graphWidth, scope.graphHeight - 100);
                ctx.stroke();





                ctx.fillStyle = "#FF0000";
                ctx.font = "9px Arial";
              
                scope.$watch('graphData', function (graphData) {

                    var d = graphData;
                    for (var i = 0; i < d.length; i++) {
                       
                      
                        ctx.fillRect(x_space, scope.graphHeight - 100, 10, -d[i].y_axis);
                        ctx.strokeStyle = "black";
                        ctx.strokeText(d[i].name, x_space, scope.graphHeight - 90);
                        ctx.strokeText(d[i].y_axis, x_space, 20);
                        x_space += 40;
                    };

                });

               

            }
        }
        
    })
    


    function FirstController($scope, $interval) {
        $scope.msg = "Hello Message!";
        $scope.graphData = [{ 'name': 'Bar A', 'y_axis': 50 }, { 'name': 'Bar B', 'y_axis': 10 }, { 'name': 'Bar C', 'y_axis': 80 }, { 'name': 'Bar D', 'y_axis': 100 }]
       
        
        var signalRConn = $.connection.myHub;

        $.connection.hub.start().done(function () {
            signalRConn.server.firstHello();

            signalRConn.server.myQueue();
        });


        signalRConn.client.firstMsg = function (result) {
            $scope.$apply(function () {
                $scope.msgSignalR = result;
            });

        }
      
        $interval(function () {
           
            signalRConn.server.helloPooling();
        }, 1000);
        signalRConn.client.firstPooling = function (result) {
            $scope.$apply(function () {
                $scope.poolClSignalR = result;
            });

        }


        signalRConn.client.firstPoolingServ = function (result) {
            $scope.$apply(function () {
                $scope.poolServSignalR = result;
            });

        }
     
        signalRConn.client.msgMyQueue = function (result) {
            $scope.$apply(function () {
                $scope.poolMSMQSignalR = result;
            });

        }
        
    }




})();
